# output "jde_info" {
#   description = "Information on JDE instances"
#   value       = module.jdetrial_flex.jde_instances_summary
# }

# resource "null_resource" "jdeinfos" {
#   for_each = module.jdetrial_flex
#   triggers = {
#     name  = each.key
#     power = each.value
#   }
# }

output "waiting_message" {
  value = "Please wait 20 minutes while your JDE Trial environments are being configured and then you can connect ..."
}


output "jdeinfos" {
    
  value = module.jdetrial_flex.jde_instances_summary
}

output "jdepwd" {
    value = format("Your JDE password is: %s", var.jdedbuserpwd)
}

# output "ip_instances" {
    
#   value = module.jdetrial_flex.instances_summary

# }
