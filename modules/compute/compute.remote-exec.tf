/*Copyright © 2018, Oracle and/or its affiliates. All rights reserved.

The Universal Permissive License (UPL), Version 1.0*/


resource "random_integer" "rand" {
  min = 1000000000
  max = 9999999999
}

#locals {
#  remote_exec_script_enabled = "${var.remote_exec_script != "" ? 1 : 0}"
#}

resource "null_resource" "initjde" {
  count               = var.instance_count 
#  depends_on  = [${element(oci_core_instance.instance.*.id, count.index)}]
  triggers = {
    current_instance_id = "${element(oci_core_instance.instance.*.id, count.index)}"
    instance_number = "${count.index + 1}"
  }
  
  provisioner "file" {
    connection {
      type               = "ssh"
      timeout             = "${var.timeout}"
      host                = oci_core_instance.instance[count.index].public_ip
      user                = "opc"
      private_key         = "${file("${var.instance_private_key}")}"
    }
    #source        = "scripts/jdedwards.template"
    #content        = base64gzip(local.setup_jdedwards)
    content        = templatefile("${path.module}/scripts/jdedwards.template",
        {
          jdehostname = oci_core_instance.instance[count.index].display_name
          jdeip = oci_core_instance.instance[count.index].public_ip
          jdedbsyspwd = var.jdedbsyspwd
          jdedbuserpwd = var.jdedbuserpwd
          jdewlspwd = var.jdewlspwd
      })
    destination   = "/home/opc/jdedwards"
  }

  provisioner "remote-exec" {
    connection {
      type               = "ssh"
      timeout             = "${var.timeout}"
      host                = oci_core_instance.instance[count.index].public_ip
      user                = "opc"
      private_key         = "${file("${var.instance_private_key}")}"
    }

    inline = [
       "rm -f /u01/vmScripts/vm_patched",
       "sudo -i mv /home/opc/jdedwards /etc/sysconfig/jdedwards;",
       "sleep 30",
       "sudo -i nohup  /u01/vmScripts/EOne_Sync.sh -config > /home/opc/jde.out 2>&1 &",
       "sleep 1"
    ]
  }
}
