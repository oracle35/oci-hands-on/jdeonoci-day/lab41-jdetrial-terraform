terraform {
  required_version = ">= 1.0"
  required_providers {
    oci = {
      source  = "oracle/oci"
      version = ">= 4.36.0"
      # https://registry.terraform.io/providers/hashicorp/oci/4.36.0
    }
    local = {
      source  = "hashicorp/local"
      version = "2.1.0" # Latest version as June 2021 = 2.1.0.
      # https://registry.terraform.io/providers/hashicorp/local/2.1.0
    }
    random = {
      source  = "hashicorp/random"
      version = "3.1.0" # Latest version as June 2021 = 3.1.0.
      # https://registry.terraform.io/providers/hashicorp/random/3.1.0
    }
    tls = {
      source  = "hashicorp/tls"
      version = "3.1.0" # Latest version as June 2021 = 3.1.0.
      # https://registry.terraform.io/providers/hashicorp/tls/3.1.0
    }
  }
}


provider "oci" {
# Auth using API Key
  tenancy_ocid     = var.tenancy_ocid
  user_ocid        = var.user_ocid
  fingerprint      = var.fingerprint
  private_key_path = var.private_key_path
# Auth using Instance Principal
# auth = "InstancePrincipal"
  region           = var.region
}

# * This module will create a Flex Compute Instance, using default values: 1 OCPU, 16 GB memory.
# * `instance_flex_memory_in_gbs` and `instance_flex_ocpus` are not provided: default values will be applied.
module "jdetrial_flex" {
  # source = "git::https://github.com/oracle-terraform-modules/terraform-oci-compute-instance" ## use this to test directly from Github HEAD
  source = "./modules/compute"
  # general oci parameters
  compartment_ocid = var.compartment_ocid
  defined_tags     = var.defined_tags
  # compute instance parameters
  ad_number                   = var.instance_ad_number
  instance_count              = var.instance_count
  instance_display_name       = "${var.instance_display_name}${var.environment}"
  instance_state              = var.instance_state
  shape                       = var.shape
  source_ocid                 = data.oci_core_app_catalog_listing_resource_version.jdetrial_catalog_listing.listing_resource_id
  source_type                 = var.source_type
  instance_flex_memory_in_gbs = var.instance_flex_memory_in_gbs # only used if shape is Flex type
  instance_flex_ocpus         = 2                               # only used if shape is Flex type
  baseline_ocpu_utilization   = var.baseline_ocpu_utilization
  cloud_agent_plugins = {
    autonomous_linux        = "DISABLED"
    bastion                 = "ENABLED"
    vulnerability_scanning  = "DISABLED"
    osms                    = "ENABLED"
    management              = "ENABLED"
    custom_logs             = "DISABLED"
    run_command             = "ENABLED"
    monitoring              = "DISABLED"
    block_volume_mgmt       = "DISABLED"
    java_management_service = "DISABLED"
  }
  # operating system parameters
  ssh_public_keys = var.ssh_public_keys
  # networking parameters
  public_ip            = var.public_ip # NONE, RESERVED or EPHEMERAL
  assign_public_ip     = var.assign_public_ip
  subnet_ocids         = [oci_core_subnet.jdetrial_sub.id]
  primary_vnic_nsg_ids = []
  # storage parameters
  boot_volume_backup_policy  = var.boot_volume_backup_policy
  block_storage_sizes_in_gbs = var.block_storage_sizes_in_gbs
  user_data = "${base64encode(file("modules/compute/scripts/vm.cloud-config"))}"

  instance_private_key  =  var.instance_private_key
  timeout               = var.timeout
  jdedbsyspwd           = var.jdedbsyspwd
  jdedbuserpwd          = var.jdedbuserpwd
  jdewlspwd             = var.jdewlspwd
}



module "jdetrial_vcn" {
  source = "./modules/vcn"

  # general oci parameters
  vcn_cidrs  =  [lookup(var.network_cidrs, "MAIN-VCN-CIDR")]
  compartment_id = var.compartment_ocid

  # vcn parameters
  vcn_name = var.vcn_name
  lockdown_default_seclist = true # boolean: true or false
  create_internet_gateway  = true
  internet_gateway_display_name = "igw"
}

resource "oci_core_subnet" "jdetrial_sub" {
  #Required
  cidr_block     = lookup(var.network_cidrs, "WEB-SUBNET-REGIONAL-CIDR")
  compartment_id = var.compartment_ocid
  vcn_id         = module.jdetrial_vcn.vcn_id

  #Optional
  display_name               = "jdetrial-sub"
  dns_label                  = "jdetrialsub"
  prohibit_public_ip_on_vnic = false
  route_table_id             = module.jdetrial_vcn.ig_route_id
  security_list_ids = [
    oci_core_security_list.jdetrial_seclist_web.id
  ]

}

resource oci_core_security_list jdetrial_seclist_web {
  compartment_id  = var.compartment_ocid
  display_name               = "sl_jdetrial-web_${var.environment}"
  egress_security_rules {
    description      = "All all egress"
    destination      = lookup(var.network_cidrs, "ALL-CIDR")
    destination_type = "CIDR_BLOCK"
    protocol  = "6"
    stateless = "false"
  }
  egress_security_rules {
    #description = <<Optional value not found in discovery>>
    destination      = lookup(var.network_cidrs, "ALL-CIDR")
    destination_type = "CIDR_BLOCK"
    protocol  = "1"
    stateless = "false"
  }
  ingress_security_rules {
    description = "Allow HTTP traffic from Internet"
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = lookup(var.network_cidrs, "ALL-CIDR")
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "8079"
      min = "8079"
    }
  }
  ingress_security_rules {
    description = "Allow HTTPS traffic for JDE from Internet"
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = lookup(var.network_cidrs, "ALL-CIDR")
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "8080"
      min = "8080"
    }
  }
  ingress_security_rules {
    description = "Allow HTTPS"
    protocol    = "6"
    source      = lookup(var.network_cidrs, "ALL-CIDR")
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "7077"
      min = "7077"
    }
  }
  ingress_security_rules {
    description = "Allow HTTPS"
    protocol    = "6"
    source      = lookup(var.network_cidrs, "ALL-CIDR")
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "7070"
      min = "7070"
    }
  }
  ingress_security_rules {
    description = "Allow SSH"
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = lookup(var.network_cidrs, "ALL-CIDR")
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "22"
      min = "22"
    }
  }
  vcn_id         = module.jdetrial_vcn.vcn_id
}

