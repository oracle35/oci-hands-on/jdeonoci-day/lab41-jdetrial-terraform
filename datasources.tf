
resource "oci_marketplace_accepted_agreement" "jdetrial_mkplace_accepted_agreement" {
  #Required
  agreement_id    = oci_marketplace_listing_package_agreement.jdetrial_listing_package_agreement.agreement_id
  compartment_id  = var.compartment_ocid
  listing_id      = data.oci_marketplace_listing.jdetrial_mkplace.id
  package_version = data.oci_marketplace_listing.jdetrial_mkplace.default_package_version
  signature       = oci_marketplace_listing_package_agreement.jdetrial_listing_package_agreement.signature
}
resource "oci_marketplace_listing_package_agreement" "jdetrial_listing_package_agreement" {
  #Required
  agreement_id    = data.oci_marketplace_listing_package_agreements.jdetrial_mkplace_package_agreements.agreements[0].id
  listing_id      = data.oci_marketplace_listing.jdetrial_mkplace.id
  package_version = data.oci_marketplace_listing.jdetrial_mkplace.default_package_version
}

data "oci_marketplace_listings" "jdetrial_mkplace" {
  name = [var.jdetrial_marketplace_name]
  compartment_id = var.compartment_ocid
}

data "oci_marketplace_listing_package_agreements" "jdetrial_mkplace_package_agreements" {
  #Required
  listing_id      = data.oci_marketplace_listing.jdetrial_mkplace.id
  package_version = data.oci_marketplace_listing.jdetrial_mkplace.default_package_version

  #Optional
  compartment_id = var.compartment_ocid
}

data "oci_marketplace_listing_package" "jdetrial_mkplace_package" {
  #Required
  listing_id      = data.oci_marketplace_listing.jdetrial_mkplace.id
  package_version = data.oci_marketplace_listing.jdetrial_mkplace.default_package_version
#  package_version  = "Oracle-E-Business-Suite-Vision-Image-12.2.10-19C-ECC-03-11-2021"

  #Optional
  compartment_id = var.compartment_ocid
}

data "oci_marketplace_listing_packages" "jdetrial_mkplace_packages" {
  #Required
  listing_id = data.oci_marketplace_listing.jdetrial_mkplace.id

  #Optional
  compartment_id = var.compartment_ocid
}

data "oci_marketplace_listing" "jdetrial_mkplace" {
  listing_id     = data.oci_marketplace_listings.jdetrial_mkplace.listings[0].id
  compartment_id = var.compartment_ocid
}

data "oci_core_app_catalog_listing_resource_version" "jdetrial_catalog_listing" {
  listing_id       = data.oci_marketplace_listing_package.jdetrial_mkplace_package.app_catalog_listing_id
  resource_version = data.oci_marketplace_listing_package.jdetrial_mkplace_package.app_catalog_listing_resource_version
}


