# ![JD Edwards Logo](./images/oci-jde.png)

This project is a Terraform configuration that deploys automatically multiple JDE Trial Editions on [Oracle Cloud Infrastructure][oci].
In just about 20 minutes you can deploy dozen JDE Trial Editions without any manuel step.


## Getting Started with jdetrial

The repository contains the [Terraform][tf] code to create all the required resources and configures the JDE Trial environments from start to end:

  * a VCN with 1 Public Subnet and 1 Security List
  * x JDE Trial instances from the Market Place

### Prerequisites

You should have an OCI tenant and a user with sufficient privileges, an API Key created.


1. Clone the [oci-jdetrial](git-project) project

```bash
git clone https://gitlab.com/oracle35/oci-jdetrial.git
```

2. Configure your deployment by creating the file **terraform.tfvers** using the template file  **terraform.tfvars-template** and updating the variables based on your target configuration:

```shell
# OCI authentication
user_ocid         = "ocid1.user.oc1..aaxxxxxxxxxxxxxxxxxxxxxxxxx"
fingerprint       = "de:d7:b4:db:d9:97:xx:xx:xxx:xx:xx:xx:xx"
tenancy_ocid      = "ocid1.tenancy.oc1..aaxxxxxxxxxxxxxxxxxxxxxxxxx"
private_key_path  = "/path/.oci/oci_api_key.pem"
# region
region = "eu-frankfurt-1"

# Target OCI Compartment
compartment_ocid = "ocid1.compartment.oc1..aaxxxxxxxxxxx"

# compute instance parameters

# SSH Public key for JDE Trial Instance
instance_private_key = "<ENTER PATH TO PRIVATE KEY FILE>"
instance_public_sshkey = "<ENTER PUBLIC KEY VALUE>"

instance_shape = "VM.Standard.E4.Flex"

network_cidrs = {
    MAIN-VCN-CIDR                = "10.0.0.0/16"
    WEB-SUBNET-REGIONAL-CIDR     = "10.0.1.0/24"
    ALL-CIDR                     = "0.0.0.0/0"
    HOME-CIDR                    = "0.0.0.0/0"
  }

# storage parameters

boot_volume_backup_policy = "disabled"

# networking parameters

# instance_count = 2
vcn_name = "jdetrial"

environment = "wrk"

network_cidrs = {
    MAIN-VCN-CIDR                = "10.0.0.0/16"
    WEB-SUBNET-REGIONAL-CIDR     = "10.0.1.0/24"
    ALL-CIDR                     = "0.0.0.0/0"
    HOME-CIDR                    = "0.0.0.0/0"
  }


shape = "VM.Standard.E4.Flex"
instance_flex_memory_in_gbs =  "16"
public_ip = "NONE"
assign_public_ip = true
instance_display_name = "jde"
block_storage_sizes_in_gbs = []
jdetrial_marketplace_name = "JD Edwards EnterpriseOne Trial Edition"


jdedbsyspwd = "<your DB SYS password>#"
jdedbuserpwd = "<your JDE User password>"
jdewlspwd = "<your WLS password>"
```

5. Deploy the Terraform stack with the following commands:

```shell
terraform init
terraform plan

var.instance_count
  Number of JDE instances to create

  Enter a value: X

terraform apply -auto-approve

var.instance_count
  Number of JDE instances to create

  Enter a value: X
```

> In a few minutes the terraform script will give you the list of URLs for your JDE Trial environments.

> However, the JDE configuration will take a few minutes to finalize. After 20-30 minutes, try to connect to the URLs provided by the Terraform output:

```
Apply complete! Resources: 13 added, 0 changed, 0 destroyed.

Outputs:

jdeinfos = [
  "jdewrk1 http://130.xx.xx.xxx:8079/jde   Orchestrator URL: http://130.xx.xx.xxx:7070/studio   IP: 130.xx.xx.xxx",
  "jdewrk2 http://130.xx.xx.xxx:8079/jde   Orchestrator URL: http://130.xx.xx.xxx:7070/studio   IP: 130.xx.xx.xxx",
  "jdewrk3 http://130.xx.xx.xxx:8079/jde   Orchestrator URL: http://130.xx.xx.xxx:7070/studio   IP: 130.xx.xx.xxx",
]
jdepwd = "Your JDE password is: xxxxxxx"
waiting_message = "Please wait 20 minutes while your JDE Trial environments are being configured and then you can connect ..."

```

### Cleanup

If you want to terminate the application and remove all OCI resources, run the following Terraform command:

```shell
terraform destroy  

var.instance_count
  Number of JDE instances to create

  Enter a value: X

# Enter yes to validate when prompted
```

#### Topology

The following diagram shows the topology created by this project.


# ![OCI-JDE Infra](./images/ocijdetrial-architecture.png)



[oci]: https://cloud.oracle.com/en_US/cloud-infrastructure
[oci-cost]: https://www.oracle.com/in/cloud/cost-estimator.html
[tf]: https://www.terraform.io
[git-project]: https://gitlab.com/oracle35/oci-hands-on/jdeonoci-day/lab4-jdetrial-terraform


### Many Thanks

For this project I reused and customized some existing Terraform modules for the compute and VCN. Many thanks for all the contributors to those modules; you can find the source on github:

* [Oracle Cloud Infrastructure Terraform Module for Compute Instance](https://github.com/oracle-terraform-modules/terraform-oci-compute-instance)
* [Terraform VCN for Oracle Cloud Infrastructure](https://github.com/oracle-terraform-modules/terraform-oci-vcn)
